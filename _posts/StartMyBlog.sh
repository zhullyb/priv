_date=$(date +%Y-%m-%d)
_url=$(kdialog --inputbox url)
_title=$(kdialog --inputbox title)
_tags=$(kdialog --inputbox tags)
touch "$_date-$_url.md"
echo "---
layout:     post
title:      $_title
date:       $_date
author:     竹林里有冰
header-img: img/2.png
catalog: true
tags:       $_tags
---

" > "$_date-$_url.md"
